#!/usr/bin/env python
from index_fitting import IndexFitting
from datetime import datetime

if __name__ == '__main__':
  start_time = datetime.now()
  print("Start at: %s" % start_time.strftime("%Y-%m-%d %H:%M:%S"))
  index_fitting = IndexFitting('500', ['CSI300', '399005', '399001', '399006'], 'index_weight.conf')
  end_time = datetime.now()
  print("Data prepared: %s" % end_time.strftime("%Y-%m-%d %H:%M:%S"))
  print("Time used: %d seconds" % (end_time - start_time).seconds)

  coeff = index_fitting.fit()
  print("The best coefficient: %s" % coeff)
  end_time = datetime.now()
  print("Start at: %s" % start_time.strftime("%Y-%m-%d %H:%M:%S"))
  print("End at: %s" % end_time.strftime("%Y-%m-%d %H:%M:%S"))
  print("Time used: %d seconds" % (end_time - start_time).seconds)
