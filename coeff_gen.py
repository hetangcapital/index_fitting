#!/usr/bin/env python
scope = 200
upper_bound = 2.0

def coeff_generator(n):
  i = 0
  while i < scope ** n:
    result = get_coeff_by_num(i, n)
    if sum(result) <= upper_bound:
      yield result
    i += 1
  return

def get_coeff_by_num(num, coeff_number):
  coeffs= [0] * coeff_number
  while coeff_number > 0 and num > 0:
    coeff = num % scope
    num //= scope
    if coeff < 1e-02:
      coeff = 0
    coeffs[coeff_number-1] = coeff
    coeff_number -= 1
  return [ i * upper_bound / scope for i in coeffs]
