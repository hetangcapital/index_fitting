#!/usr/bin/env python
import sys
import copy
import easyquotation
from coeff_gen import *

class IndexFitting:
  #read weights for indices from configuration
  def __init__(self, b_index_name = 'big_index', s_index_names = ['index1', 'index2'], cfg_name = 'example.conf'):
    self.quotation = easyquotation.use('sina').all

    self.all_index_comp = {}
    self.b_index_name = b_index_name #big index name
    self.s_index_names = s_index_names #small index names, a list

    with open(cfg_name) as f_cfg:
      lines = f_cfg.readlines()
      row = 0
      while row < len(lines):
        title = lines[row].split('#')
        if len(title) > 2: #Encounter ###
          break
        index_name = title[0].strip()
        self.all_index_comp[index_name] = {}
        components = lines[row + 1].strip().split(',')
        weights = lines[row + 2].strip().split(',')
        assert len(components) == len(weights)
        for i in range(0, len(components)):
          self.all_index_comp[index_name][components[i]] = float(weights[i])
        self.normalize(self.all_index_comp[index_name])
        row += 3

  def get_price(self, ticker):
    return self.quotation[ticker]['now']

  #normalize the weights of componetial stocks in one index
  def normalize(self, index_component):
    total_weight = 0
    for ticker, weight in index_component.items():
      total_weight += weight * self.get_price(ticker)
    for ticker, weight in index_component.items():
      index_component[ticker] = weight * self.get_price(ticker) / total_weight

  #apply the coefficient to componential stocks
  def weighting(self, coeff_map): #coeff_map: index name to coeff
    result = {}
    for index_name, coeff in coeff_map.items():
      assert index_name in self.all_index_comp
      for ticker, weight in self.all_index_comp[index_name].items():
        result.setdefault(ticker, 0)
        result[ticker] += weight * coeff
    return result
      
  #calculate the difference between two index components
  @staticmethod
  def difference(index_a, index_b):
    components_diff = {}
    tickers = set()
    tickers.update(index_a.keys())
    tickers.update(index_b.keys())
    for ticker in tickers:
      index_a.setdefault(ticker, 0.0)
      index_b.setdefault(ticker, 0.0)
      diff = index_a[ticker] - index_b[ticker]
      if abs(diff) < 1e-6:
        continue
      components_diff[ticker] = diff
    return components_diff
  
  #error function, the lower point the better
  def err_func(self, index_a, index_b):
    components_diff = IndexFitting.difference(index_a, index_b)
    point = 0.0
    for ticker, weight in components_diff.items():
      point += self.get_price(ticker) * abs(weight)
#      point += abs(weight)
    return point
  
  #use coeff_generator to iterate all possible coefficients
  def fit(self):
    opt_point = 10000.0 #initial point
    num = len(self.s_index_names)
    for coeff in coeff_generator(num): #coeff is a list
      coeff_map = {} #hash: index name to its coefficient
      for k, v in zip(self.s_index_names, coeff):
        coeff_map[k] = v
      result_index = self.weighting(coeff_map)
      point = self.err_func(result_index, self.all_index_comp[self.b_index_name])
      if point < opt_point:
        print("Found better coeff: %s, point: %f" % (coeff, point))
        opt_point = point
        opt_coeff = coeff
    return opt_coeff
